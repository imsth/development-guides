# Development Information for IMS Messsysteme GmbH #


Just some guides to point developers in the right direction.

## Supported OS ##
Windows 7 and greater

Windows Server 2008 and greater

Linux Ubuntu 14.04 and greater


## Configuration Files ##
All persistent configuration files for applications should be located in: `%ALLUSERSPROFILE%/IMS Messsysteme/<product>/` or for Linux: `/etc/IMS Messsysteme/<product>/`

Where `<product>` is the service name.

## Log Files ##
All persistent logging files for applications should be located in: `c:/IMSLogFiles/<product>/` or for Linux: `/var/log/IMSLogFiles/<product>/`

Where `<product>` is the service name.


## Set Developer Machine ##
to switch console output. Add an envioment setting 
add ```IMS_DEVELOPER_MACHINE = 1``` to the envioment settings. 

How to do it checkout here http://www.computerhope.com/issues/ch000549.htm

## Folder structure for projects ##

```
*** => .gitignore

.idea
.vscode
.eslintignore
.eslintrc.json
.gitignore
.editorconfig
package.json
gulpfile.js

doc  
  flowDesignImage.jpg
  allLicense.csv

dist
  *.7z

node_modules
   .bin
      node.exe
      npm.cmd
      7zip.exe
      7zip.dll
      nssm.exe
src
  manual
    htmlHelpProject
  client
    manual ***
      htmlHelpResult 
    lib
      js
      css
    js
      controller
      service
      filter
      directive
      app.js
    css
      *.css|(*.less|*.sass)
    template
      *.html
    image
      favicon.ico
      imsLogo.svg 
    font
    index.html
  server
    rest
    configuration
    server.js

test
  client
  server
  e2e
    rest
    dashboard
    comparision

task
  *.cmd
```

## Tools ##
* NodeJS https://nodejs.org/dist/v6.9.2/node-v6.9.2-x86.msi => currently used javascript version is: ECMAScript 5.1 (ES5), NodeJS 6.9.x and nearly all modern browsers support this version, feature compatibility table: http://kangax.github.io/compat-table moving to NodeJS 5.x is on the way... (moving to ES6 is possible, when Chrome, Firefox, Safari, and Edge support nearly all features)
* install `Visual Studio 2015 with C++` **or** `VisualCppBuildTools2015`, both are there: \\sbnas01\share\install\MSDN\VisualStudio) 
* git: https://git-scm.com/downloads
* python: https://www.python.org/ftp/python/2.7.10/python-2.7.10.msi or any 2.7.x **but not 3.x => it is not supported!**
* IMORTANT: Visual Studio 2010 Redistributables for C++ (for ZMQ) => \\sbnas01\share\install\MSDN\VisualStudio\VS2010\vcredist_x86.exe
*** The target system may need all CPP redistributables or else the programs will silently fail to start !**

## JavaScript Style Guide ###
https://github.com/airbnb/javascript/tree/es5-deprecated/es5

For basic code style we use http://editorconfig.org/. The current .editorconfig file is also in this repro.

## Problems with corporate proxy

create files in your user directory (example: C:\Users\wb):
```
.npmrc:
proxy=http://192.168.10.9:3128/
https-proxy=http://192.168.10.9:3128/
##registry=https://registry.npmjs.org
##registry=http://m-w7-opti-th:4873/

.gitconfig:
[http]
	proxy = http://192.168.10.9:3128
[url "https://"]
	insteadOf = git://
[https]
	proxy = http://192.168.10.9:3128 

.bowerrc:
{
    "directory": "app/bower_components",
    "registry": "http://bower.herokuapp.com",
    "proxy":"http://192.168.10.9:3128/",
    "https-proxy":"http://192.168.10.9:3128/",
    "strict-ssl": false
}
```
---

## Git Workflow ##
GIT-Guide: http://rogerdudler.github.io/git-guide/

Checkout project

* open git bash
* change directory (cd Project/Bitbucket/)
* checkout project (git clone https://imswb@bitbucket.org/imsgmbh/fileporter.git FilePorter)
* change directory to the project (cd FilePorter)
* install the used packages (npm install)

----
For each feature:

Get newest files

* git pull
* gulp test

Create branch for feature "EnoughStorageInDestinationFolder" and switch to it at the same time

* git checkout -b EnoughStorageInDestinationFolder

Develop new feature

* gulp test

* git status
* git add readme.md
* git commit -m "dies und das"

Push feature to remote repository

* git push -u origin EnoughStorageInDestinationFolder

Create Pullrequest from branch in Bitbucket

Checkout the master and merge with the newest features

* git checkout master

* git pull

## Create new repositiory and first commit ##
* create a BitBucket repository, owner is: imsgmbh
* mkdir /path/to/your/project
* cd /path/to/your/project
* git init
* git remote add origin https://yourname@bitbucket.org/imsgmbh/reponame.git


## Merge Conflicts ##
* checkout your branch: git checkout mybranch
* merge with master: git merge master
* edit conflicted files to contain the right code
* resolve conflict by adding all edited files: git add .
* commit the files: git commit -m "conflict resolved"
* git push -u origin mybranch

---

## Tests ##
Coverage is measured with: testCoverage.cmd

Coverage is validated with: validateCoverage.cmd

gulpfile.js should contain global thresholds for istanbul: 80

so that a coverage rate of 80% is reached for all lines, functions, branches and statements.


Command line options always have highest priority.
Permanent changes to standard options should be applied to ServiceManager.
Temporary changes can be applied to package.json and will be overwritten by software update.

Service-Client-Mocks can be loaded at runtime (command line option) instead of real implementation:
-MOCKML mlClientMock.js
-MOCKSP spClientMock.js

## Issue Workflow ##
1. Move issue in Jira to *In Progress*
2. Create branch for issue with name *[IssueIdInJira]* (example: MMSG-238)
3. Work on issue and commit/push to branch
4. Move issue in Jira to *Review* and create Merge Request in GitLab with titel *[IssueIdInJira] - [IssueDescInJira]*
5. Wait for other developer to review pull request and fix/change until approved and merged
6. move issue to *Done*

---

## Multi Runner ##

Details here: https://gitlab.com/gitlab-org/gitlab-ci-multi-runner

### Install on Windows ###

Create a folder somewhere in your system, ex.: ```C:\Multi-Runner.```
Download the binary for x86 or amd64 and put it into the folder you created.
Run an Administrator command prompt (How to). 
The simplest is to write ```Command Prompt``` in Windows search field, right click and select Run as ```administrator```. 
You will be asked to confirm that you want to execute the elevated command prompt.

### Register the runner: ###
```
cd C:\Multi-Runner
gitlab-ci-multi-runner register

Please enter the gitlab-ci coordinator URL (e.g. http://gitlab-ci.org:3000/ )
https://ci.gitlab.com
Please enter the gitlab-ci token for this runner
### xxx ###
Please enter the gitlab-ci description for this runner
my-runner
INFO[0034] fcf5c619 Registering runner... succeeded
Please enter the executor: shell, docker, docker-ssh, ssh?
shell
INFO[0037] Runner registered successfully. Feel free to start it, but if it's
running already the config should be automatically reloaded!
```

### how to find gitlab-ci token ```xxx``` ###
```
1. goto reprository
2. open right upper side properties dialog
3. select runners
4. find token and rember it everery time!
```

### Runner Registration ###
```
gitlab-ci-multi-runner-windows-386.exe register \
--non-interactive \
--url "https://gitlab.com/ci" \
--registration-token "token from project => configuration button top right, choose runner" \
--description "This is my runner on my machine" \
--executor "shell"
```

### Start runner with Runner.cmd ###
```
set HTTP_PROXY=http://192.168.10.9:3128
set HTTPS_PROXY=http://192.168.10.9:3128
set NO_PROXY=127.0.0.1
gitlab-ci-multi-runner-windows-386.exe run 
```

### Sample config.toml (will be created from registration process) ###
```
.config.toml 
concurrent = 1
check_interval = 0
[[runners]]
name = "Th1-Runner-W7-32"
url = "https://gitlab.com/ci"
token = "1234567890abcdef..."
executor = "shell"
builds_dir = "C:/builds"
[runners.ssh]
[runners.docker]
tls_verify = false
image = ""
privileged = false
disable_cache = false
[runners.parallels]
base_name = ""
disable_snapshots = false
[runners.virtualbox]
base_name = ""
disable_snapshots = false
[runners.cache]
Insecure = false 
```

The output of runner file includes some ansi color sequence. 
To get a better result install ```ansicon``` https://github.com/adoxa/ansicon 

#### ANSICON ####

ANSICON provides ANSI escape sequences for Windows console programs.  It
provides much the same functionality as `ANSI.SYS` does for MS-DOS.

##### Requirements #####

* 32-bit: Windows 2000 Professional or later (it won't work with NT or 9X).
* 64-bit: Vista or later (it won't work with XP64).

##### Installation #####

Add `x86` (if your OS is 32-bit) or `x64` (if 64-bit) to your `PATH`, or copy
the relevant files to a directory already in the `PATH`.

Alternatively, use option `-i` (or `-I`, if permitted) to install it
permanently, by adding an entry to `CMD.EXE`'s AutoRun registry value (current
user or local machine, respectively).


### Requirements to use the runner as IMS Build tool###
```
- Install .NET Framework >= 4.5.1
- install VisualCppBuildTools2015 (\\sbnas01\share\install\MSDN\VisualStudio)
- install CPP redistributables for VS2010 (\\sbnas01\share\install\MSDN\VisualStudio)
```
----

## Shrinkwrap ##
"npm install" could be dangerous because of changed modules.

"npm shrinkwrap" will take the version numbers of all currently installed node_modules 
and write them to npm-shrinkwrap.json.

"npm install" will then use those version numbers to install missing modules.

It must be done in the fololowing order:
```
1) remove npm-shrinkwrap.json
2) remove node_modules: rd /S /Q node_modules
3) remove it again (sometimes files or directories are left): rd /S /Q node_modules
4) get node and npm: task\getNode.cmd
5) install production only: task\npmInstallProduction.cmd
6) install the rest: task\installNpm.cmd
7) create npm-shrinkwrap.json: npm shrinkwrap

If problems arise try "npm cache clean" and repeat the whole process.

```


